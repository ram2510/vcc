var sliderSelector = '.s1',
options = {
    // init: false,
    loop: true,
    speed: 800,
    spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 50,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    effect: 'cube', // 'cube', 'fade', 'coverflow',
    // grabCursor: true,
    // Events
    on: {
        imagesReady: function () {
            this.el.classList.remove('loading');
        }
    }
};
var mySwiper = new Swiper(sliderSelector, options);

// Initialize slider
mySwiper.init();

