document.getElementById('comp').value = sessionStorage.getItem('item')
console.log(document.getElementById('comp').value==="Perception")
if(document.getElementById('comp').value==="Perception") {
  document.getElementById('college').value="VIT";
  document.getElementById('college').readOnly=true

}

let optionData = [{comp:"Frame It",pay:100},{comp:"Perception",pay:50}].find(item=>item.comp===sessionStorage.getItem('item'))
document.getElementById('pay').value = optionData.pay;
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

/**
 * @param {string} paymentSite - Payment site.
 * @param {number} amount - Price of event.
 * @param {string} callback - Return link to caller website (mandatory to be a URI).
 * @param {string} eventType - Type of event.
 * @param {srting} id - Event Identifier.
 */
function pay(paymentSite, amount, callback, eventType, id) {
  window.location.href = paymentSite 
                      + amount + "/" 
                      + eventType + "/" 
                      + id + "/"
                      + encodeURIComponent(callback);
}
